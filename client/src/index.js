import React from 'react';
import ReactDOM from 'react-dom'
import { Provider } from 'react-redux'
import { Router } from 'react-router'
import history from './history'

import { MuiThemeProvider, createMuiTheme } from '@material-ui/core/styles'

import './index.css'
import App from './App/index'
import store from './store/rootStore'


const theme = createMuiTheme({
    overrides: {
        MuiButton:{
            sizeSmall: {
                fontSize: "12px",
            },
        },
        PersistentDrawer: {
            appFrame: {
                width: '50px' ,
                height: '50px',
            }
        },
        MuiInput: {
            root: {
                fontSize: "14px"
            },
            underline: {
                '&::before': {
                    display: 'none'
                },
                '&::after': {
                    display: 'none'
                }
            }
        },
        MuiNativeSelect: {
            root: {
                backgroundColor: 'white'
            },
            select: {
                padding: '5px 10px',
                '&:focus': {
                    backgroundColor: 'white'
                }
            }
        },
        MuiTypography: {
            subheading: {
                fontSize: "12px",
                
            }
        },
        MuiListItem: {
            default: {
                'padding-top': '6px',
                'padding-bottom': '6px',
            }
        }
        
      },
});

  
ReactDOM.render(
    <Router history={history}>
        <MuiThemeProvider theme={theme}>
            <Provider store={store}>
                <App />
            </Provider>
        </MuiThemeProvider>
    </Router>, 
document.getElementById('root'));







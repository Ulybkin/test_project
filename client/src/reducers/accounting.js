import {
    FETCH_DATA_FOR_GRATHS_SUCCEEDED
} from '../constants/menu'
  
const initialState = []

export default function accounting(state = initialState, action) {
const {type, payload} = action
switch (type) {
    case FETCH_DATA_FOR_GRATHS_SUCCEEDED:
        return payload
    default:
        return state
    }
}

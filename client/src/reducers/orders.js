import {
    FETCH_ORDERS_SUCCEEDED
} from '../constants/menu'
  
const initialState = {
    customers: {},
    goodsInfo: {},
    ordersInfo: {},
    orderIds: []
}

export default function orders(state = initialState, action) {
const {type, payload} = action
switch (type) {
    case FETCH_ORDERS_SUCCEEDED:
        return payload
    default:
        return state
    }
}

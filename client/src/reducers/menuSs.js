import {
    FETCH_SS_DATA_SUCCEEDED 
} from '../constants/menu'
  
const initialState = {
    companyIds: [],
    ssPeople: {},
    scoringForAnEmployee: {},
    companyInfo: {}
}

export default function menuSs(state = initialState, action) {
const {type, payload} = action
switch (type) {
    case FETCH_SS_DATA_SUCCEEDED:
        return payload
    default:
        return state
    }
}

// @flow
import { combineReducers } from "redux";

import menuPeople from './menuPeople'
import menuSs from './menuSs'
import orders from './orders'
import accounting from './accounting'

const rootReducer = combineReducers({
  menuPeople,
  menuSs,
  orders,
  accounting
});

export default rootReducer;

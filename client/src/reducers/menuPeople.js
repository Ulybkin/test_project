import {
    FETCH_PEOPLE,
    FETCH_PEOPLE_SUCCEEDED
} from '../constants/menu'
  
const initialState = []

export default function menuPeople(state = initialState, action) {
const {type, payload} = action
switch (type) {
    case FETCH_PEOPLE:
        return state
    case FETCH_PEOPLE_SUCCEEDED:
        return payload
    default:
        return state
    }
}

import { all, call, put, takeEvery } from 'redux-saga/effects'

import { FETCH_ORDERS } from '../constants/menu'
import { fetchOrdersSucceed } from '../actions/orders'

import { apiGetOrders } from '../api'

function* orders() {
    yield all([
        takeEvery(FETCH_ORDERS, function*() {
            try {
                const data = yield call(apiGetOrders)
                yield put(fetchOrdersSucceed(data));
            } catch(error) {
                console.log("печаль-беда")
            }
        })
    ])
} 
  
export default orders;
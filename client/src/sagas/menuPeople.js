import { all, call, put, takeEvery } from 'redux-saga/effects'

import { FETCH_PEOPLE } from '../constants/menu'
import { fetchMenuPeopleSucceed } from '../actions/menuPeople'

import { apiGetPeople } from '../api'

function* menuPeople() {
    yield all([
        takeEvery(FETCH_PEOPLE, function*() {
            try {
                const data = yield call(apiGetPeople)
                yield put(fetchMenuPeopleSucceed(data));
            } catch(error) {
                console.log("печаль-беда")
            }
        })
    ])
} 
  
export default menuPeople;
import { all, call, put, takeEvery } from 'redux-saga/effects'

import { FETCH_DATA_FOR_GRATHS } from '../constants/menu'
import { fetchDataForGrathsSucceed } from '../actions/accounting'

import { apiGetDataForGraths } from '../api'

function* accounting() {
    yield all([
        takeEvery(FETCH_DATA_FOR_GRATHS, function*() {
            try {
                const data = yield call(apiGetDataForGraths)
                yield put(fetchDataForGrathsSucceed(data));
            } catch(error) {
                console.log("печаль-беда")
            }
        })
    ])
} 
  
export default accounting;
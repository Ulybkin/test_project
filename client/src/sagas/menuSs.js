import { all, call, put, takeEvery } from 'redux-saga/effects'

import { FETCH_SS_DATA } from '../constants/menu'
import { fetchSsDataSucceed } from '../actions/menuSs'

import { apiGetSsData } from '../api'

function* menuSs() {
    yield all([
        takeEvery(FETCH_SS_DATA, function*() {
            try {
                const data = yield call(apiGetSsData)
                yield put(fetchSsDataSucceed(data));
            } catch(error) {
                console.log("печаль-беда")
            }
        })
    ])
} 
  
export default menuSs;
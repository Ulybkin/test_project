import { all, fork } from "redux-saga/effects"

import menuPeople from "./menuPeople"
import menuSs from "./menuSs"
import orders from "./orders"
import accounting from "./accounting"

export default function rootSaga() {
    return function*() {
        yield all([
            fork(menuPeople),
            fork(menuSs),
            fork(orders),
            fork(accounting),
        ]);
    };
}
  
// @flow

export const apiGetPeople = () =>
  fetch("/api/people")
    .then((res) => res.json())
    .then(people => people);

export const apiGetSsData = () =>
  fetch("/api/ssData")
    .then((res) => res.json())
    .then(ssData => ssData);

export const apiGetOrders = () =>
  fetch("/api/orders")
    .then((res) => res.json())
    .then(ssData => ssData);

  export const apiGetDataForGraths = () =>
  fetch("/api/dataForGraths")
    .then((res) => res.json())
    .then(data => data);
    
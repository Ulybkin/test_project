import React from 'react'
import PropTypes from 'prop-types'
import { Container } from "./styled"


const Info = ({id, date}) => (
    <Container>
        Заказ № <a href="#">{id}</a> от {date}
    </Container>
)

Info.propTypes = { 
    id: PropTypes.string,
    date: PropTypes.string,
}

export default Info
  
import styled from "styled-components";

export const Container =  styled.div`
    font-family: sans-serif;
    a {
        display: inline-block;
        color: blue;
    }

    @media (max-width: 855px) {
        font-size: 12px;
    }
    
`;

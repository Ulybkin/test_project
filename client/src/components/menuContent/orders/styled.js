import styled from "styled-components";

export const Container =  styled.div`
    font-family: sans-serif;
    display: grid;
    grid-template-columns: minmax(320px, 750px);
    justify-content: center;
    background-color: white;

    h1 {
        text-align: center;
        color: #222;
    }

    a {
        display: block;
        text-align: center;
        color: #222;
    }

    @media (max-width: 855px) {
        margin-top: 40px;
      }
    

`;
import React from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'

import Info from '../info'
import CustomerInfo from '../customerInfo'
import Good from '../good'
import { Container } from "./styled"
import {
  getOrderInfoById
} from '../../../../selectors/orders'

import Divider from '@material-ui/core/Divider'
import ExpansionPanel from '@material-ui/core/ExpansionPanel';
import ExpansionPanelSummary from '@material-ui/core/ExpansionPanelSummary';
import ExpansionPanelDetails from '@material-ui/core/ExpansionPanelDetails';

const Panel = ({id, orderInfo}) => {
  return( 
    <Container>
      <ExpansionPanel>
        <ExpansionPanelSummary className="summary">
          <Info id={id} date={orderInfo.date} />
        </ExpansionPanelSummary>
        <ExpansionPanelDetails className="detail">
          <CustomerInfo customerId={orderInfo.customerId}/>
          {orderInfo.goods.map((goodId, index) => ([
            <Divider key={index}/>,
            <Good key={goodId} goodId={goodId}/> 
          ]))}
        </ExpansionPanelDetails>
      </ExpansionPanel>
    </Container>
 )
}

Panel.propTypes = {
  id: PropTypes.string,
  //connect
  orderInfo: PropTypes.object
}

const mapStateToProps = (state, ownProps) => {
  return {
    orderInfo: getOrderInfoById(state, ownProps.id)
  }
}

export default connect(mapStateToProps)(Panel)

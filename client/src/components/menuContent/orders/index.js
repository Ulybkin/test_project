import React, {Component} from 'react'
import PropTypes from 'prop-types';
import { connect } from 'react-redux'

import { Container } from "./styled"
import { fetchOrders } from '../../../actions/orders'
import { getOrderIds } from '../../../selectors/orders'
import Panel from "./panel"

class Orders extends Component {
  componentDidMount() {
    this.props.fetchOrders()
  }

  render() {
    const { orderIds } = this.props
    const personBlocksControl = orderIds.map((id) => (<Panel key={id} id={id}/>))
    
    return( 
      <Container>
        {personBlocksControl}
      </Container>
   )
  }
}

Orders.propTypes = {
  //redux
  orderIds: PropTypes.array
}

const mapStateToProps = (state) => {
  return {
    orderIds: getOrderIds(state)
  }
}

export default connect(mapStateToProps, {fetchOrders})(Orders)
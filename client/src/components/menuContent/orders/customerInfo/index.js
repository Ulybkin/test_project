import React from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'

import { Container } from "./styled"
import { customerFullNameGetter } from '../../../../selectors/orders'



const CustomerInfo = ({customerFullName}) => (
    <Container>
        <a href="#">{customerFullName}</a>
    </Container>
)

CustomerInfo.propTypes = { 
    customerId: PropTypes.string,
    //connect
    customerFullNameGetter: PropTypes.string,
}

const mapStateToProps = (state, ownProps) => {
    return {
        customerFullName: customerFullNameGetter(state, ownProps.customerId)
    }
  }
  
export default connect(mapStateToProps)(CustomerInfo)
  
  
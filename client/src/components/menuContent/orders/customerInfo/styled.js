import styled from "styled-components";

export const Container =  styled.div`
    font-family: sans-serif;
    font-size: 12px;
    a {
        display: inline-block;
        color: blue;
    } 
`;

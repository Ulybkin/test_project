import styled from "styled-components";

export const Container =  styled.div`
    font-family: sans-serif;
    display: flex;
    flex-flow: row nowrap;
    justify-content: space-between;
    align-items: center;

    & div {
        display: flex;
        flex-flow: row nowrap;
        justify-content: space-between;
        align-items: center;

        & img {
            margin-right: 10px;
        }
    }

    & .score {
        padding-right: 25px;
    }

    @media (max-width: 855px) {
        font-size: 12px;
    }

    @media (max-width: 452px) {
        flex-flow: column nowrap;
        justify-content: flex-start;
        align-items: flex-start;
    }

`;

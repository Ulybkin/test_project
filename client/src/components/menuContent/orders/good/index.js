import React from 'react'
import PropTypes from 'prop-types'
import {connect} from 'react-redux'

import klyuch from '../../../../images/klyuch.png'
import car_charger from '../../../../images/car_charger.png'
import screwdriver from '../../../../images/screwdriver.png'
import { Container } from "./styled"
import { goodInfoByIdGetter } from '../../../../selectors/orders'

const Good = ({ goodInfo }) => {
    return(
        <Container>
            <div>
                <img 
                src={
                    goodInfo.type === "car_charger" ? car_charger :
                    goodInfo.type ===  "klyuch" ? klyuch :
                    goodInfo.type ===  "screwdriver" ? screwdriver : screwdriver
                } 
                width= '40px'
                height= '40px'
                />
                <span>{goodInfo.description}</span>
            </div>
            <span className='score'>{`${goodInfo.score} баллов`}</span>
        </Container>
   )
}

Good.propTypes = { 
    goodId: PropTypes.string,
    //connect
    goodInfo: PropTypes.object
}


const mapStateToProps = (state, ownProps) => {
    return {
        goodInfo: goodInfoByIdGetter(state, ownProps.goodId)
    }
}
  
export default connect(mapStateToProps)(Good)



  
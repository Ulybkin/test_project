import styled, { css } from "styled-components";

const btn = (light, dark) => css`
  white-space: nowrap;
  display: inline-block;
  border-radius: 5px;
  padding: 5px 15px;
  font-size: 16px;
  color: white;
  &:visited {
    color: white;
  }
  background-image: linear-gradient(${light}, ${dark});
  border: 1px solid ${dark};
  &:hover {
    background-image: linear-gradient(${light}, ${dark});
    &[disabled] {
      background-image: linear-gradient(${light}, ${dark});
    }
  }
  &:visited {
    color: black;
  }
  &[disabled] {
    opacity: 0.6;
    cursor: not-allowed;
  }
`;

const btnDefault = css`${btn("#ffffff", "#d5d5d5")} color: #555;`;

const btnPrimary = btn("#4f93ce", "#285f8f");

export default styled.div`
  font-family: sans-serif;

  h1 {
    text-align: center;
    color: #222;
  }

  & > div {
    text-align: center;
  }

  a {
    display: block;
    text-align: center;
    color: #222;
  }

  form {
    width: 613px;
    border: 1px solid #ccc;
    padding: 20px;
    box-shadow: 2px 2px 5px rgba(0, 0, 0, 0.3);
    border-radius: 3px;
    background-color: white;

    & > div {
      display: flex;
      flex-flow: column nowrap;
      line-height: 2em;
      margin: 5px;
      & > label {
        color: #333;
        width: 100%;
        font-size: 1em;
        line-height: 32px;
      }
      & > input,
      & > select,
      & > textarea {
        flex: 1;
        padding: 3px 5px;
        font-size: 1em;
        border: 1px solid #ccc;
        border-radius: 3px;
      }
      & > input[type="checkbox"] {
        margin-top: 7px;
      }
      & > div {
        margin-left: 16px;
        & > label {
          display: block;
          & > input {
            margin-right: 3px;
          }
        }
      }
    }
    & .buttons {
      display: flex;
      flex-flow: row nowrap;
      justify-content: center;
      margin-top: 15px;
    }
    & > button {
      margin: 0 10px;
      &[type="submit"] {
        ${btnPrimary};
      }
      &[type="button"] {
        ${btnDefault};
      }
    }
  }

  .menuOpen {
    margin: -50px;
  }

  .upperBlock{ 
    display: flex;
    flex-flow: row nowrap;
    justify-content: space-between; 
    & p {
      margin: 0;
    }
    & div {
      display: flex;
      flex-flow: row nowrap;
      margin: 0;
      border-radius: 4px;
      border: 1px solid #ccc;
      overflow:hidden;
      & button {
        background-image: none;
        font-size: 12px;
        margin: 0px;
        border-radius: 0px;
        border-right: 1px solid #ccc;
        border-left: 0px;
        border-top: 0px;
        border-bottom: 0px;
        &:hover {
          background-image: none;
        }
      }
      & button:last-child {
        border-right: 0px;
      }
      
    }
  }



`;

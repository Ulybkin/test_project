import React from 'react';
import { Form, Field } from 'react-final-form'

import Styles from './styles'

import MaterialButton from "../../../common/button"

const sleep = ms => new Promise(resolve => setTimeout(resolve, ms))

const onSubmit = async values => {
  await sleep(300)
  window.alert(JSON.stringify(values, 0, 2))
}


const Content = () =>  {
    return(
      <Styles>
        <Form
          onSubmit={onSubmit}
          initialValues={{ employed: true, stooge: 'larry' }}
          className= {""}
          render={({ handleSubmit, reset, submitting, pristine, values }) => (
            <form onSubmit={handleSubmit}>
              <div className="upperBlock">
                <p>Баллы: 30</p>
                <div>
                  <MaterialButton text="Удалить" size="small" />
                  <MaterialButton text="Зачислить баллы" size="small" />
                  <MaterialButton text="Обнулить" size="small" />
                </div>
              </div>
              <div>
                <label>Серийный номер</label>
                <Field
                  name="firstName"
                  component="textArea"
                  type="text"
                />
              </div>
              <div>
                <label>Brand</label>
                <div>
                  <label>
                    <Field
                      name="brand"
                      component="input"
                      type="radio"
                      value="brand 1"
                    />{' '}
                    Brand 1
                  </label>
                  <label>
                    <Field
                      name="brand"
                      component="input"
                      type="radio"
                      value="Brand 2"
                    />{' '}
                    Brand 2
                  </label>
                </div>
              </div>
              <div>
                <label>Вид работ</label>
                <Field name="workType" component="select">
                  <option />
                  <option value="Пусконаладка">Пусконаладка</option>
                  <option value="Отладка">Отладка</option>
                  <option value="Ремонт">Ремонт</option>
                </Field>
              </div>
              <div>
                <label>Фирма-установщик</label>
                <Field
                  name="firmInstaller"
                  component="input"
                  type="text"
                />
              </div>
              <div>
                <label>ФИО владельца(полностью)</label>
                <Field
                  name="FIO"
                  component="input"
                  type="text"
                  placeholder="Иванов Иван Иванович"
                />
              </div>
              <div>
                <label>Телефон владельца</label>
                <Field
                  name="phoneNumber"
                  component="input"
                  type="text"
                  placeholder="+7(233)323-23-22"
                />
              </div>
              <div>
                <label>Дата пусконаладки</label>
                <Field
                  name="date"
                  component="input"
                  type="text"
                  placeholder="12.12.1222"
                />
              </div>
              <div>
                <label>ФИО специалиста, производившего пусконаладку</label>
                <Field
                  name="FIOSpecialist"
                  component="input"
                  type="text"
                  placeholder="Иванов Иван Иванович"
                />
              </div>
              <div className="buttons">
                <button type="submit" disabled={submitting || pristine}>
                  Submit
                </button>
                <button
                  type="button"
                  onClick={reset}
                  disabled={submitting || pristine}>
                  Reset
                </button>
              </div>
              
            </form>
          )}
        />
      </Styles>
   )
}


Content.propTypes = {
  
}


export default Content
  
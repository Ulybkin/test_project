import styled from "styled-components";

export const Container =  styled.div`
    font-family: sans-serif;
    flex-flow: column nowrap;
    justify-content: center; 
    & div {
        & span {
            border-bottom: 1px dotted black;
        }
    }
`;

import React from 'react'
import PropTypes from 'prop-types'
import { Container } from "./styled"


const Info = ({label, statistics}) => (
    <Container>
        <div>
            <span>{label}</span>
        </div>
        <span>{statistics}</span>
    </Container>
)

Info.propTypes = { 
    label: PropTypes.string,
    statistics: PropTypes.string,
}

export default Info
  
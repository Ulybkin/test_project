import React, {Component} from 'react'
import PropTypes from 'prop-types';
import { connect } from 'react-redux'

import { Container } from "./styled"
import { fetchSsData } from '../../../actions/menuSs'
import { getCompanyIds } from '../../../selectors/menuSs'
import Panel from "./panel"

class Ss extends Component {
  componentDidMount() {
    this.props.fetchSsData()
  }

  render() {
    const { companyIds } = this.props
    const personBlocksControl = companyIds.map((id) => (<Panel key={id} id={id}/>))
    
    return( 
      <Container>
        {personBlocksControl}
      </Container>
   )
  }
}

Ss.propTypes = {
  //redux
  companyIds: PropTypes.array
}

const mapStateToProps = (state) => {
  return {
    companyIds: getCompanyIds(state)
  }
}

export default connect(mapStateToProps, {fetchSsData})(Ss)
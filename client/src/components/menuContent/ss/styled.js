import styled from "styled-components";

export const Container =  styled.div`
    font-family: sans-serif;

    h1 {
        text-align: center;
        color: #222;
    }

    a {
        display: block;
        text-align: center;
        color: #222;
    }

    display: grid;
    grid-template-columns: minmax(320px, 750px);
    justify-content: center;

    /* border: 1px solid #ccc;
    box-shadow: 2px 2px 5px rgba(0, 0, 0, 0.3);
    border-radius: 3px; */
    background-color: white;

    @media (max-width: 855px) {
        margin-top: 40px;
    }
`;
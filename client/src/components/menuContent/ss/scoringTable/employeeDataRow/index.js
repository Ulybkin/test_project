import React from 'react'
import PropTypes from 'prop-types'
import { Row, Col } from 'react-flexbox-grid';

const EmployeeDataRow = ({date, empoyee, description, score}) => {

    return ( 
        <Row>
            <Col lg={2}>{date}</Col>
            <Col lg={3}>{empoyee}</Col>
            <Col lg={5}>{description}</Col>
            <Col lg={2}>{score}</Col>
        </Row>
    )
}

EmployeeDataRow.propTypes = {
    date: PropTypes.string,
    empoyee: PropTypes.string, 
    description: PropTypes.string, 
    score: PropTypes.number,
}

export default EmployeeDataRow

  
import styled from "styled-components";

export const Container =  styled.div`
    font-family: sans-serif;
    font-size: 12px;
    display: flex;
    flex-flow: row nowrap;
    & .columnName {
        display: flex;
        flex-flow: row nowrap;
        justify-content: space-between; 
    }
    

`;

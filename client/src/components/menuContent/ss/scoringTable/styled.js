import styled from "styled-components";


export const GridWrapper =  styled.div`
    font-size: 12px;

    @media (max-width: 990px) {
        display: none;
    }
`;

import React from 'react'
import PropTypes from 'prop-types'
import { Grid, Row, Col } from 'react-flexbox-grid';

import { GridWrapper } from "./styled"
import EmployeeDataRow from './employeeDataRow';

import Divider from '@material-ui/core/Divider';

const ScoringTable = ({scoringForAnEmployee}) => {
    return(
        <GridWrapper>
            <Grid fluid>
                <Row>
                    <Col lg={2}>Когда</Col>
                    <Col lg={3}>Кому</Col>
                    <Col lg={5}>За что</Col>
                    <Col lg={2}>Сколько</Col>
                </Row>
            </Grid>
            <Divider />
            <Grid fluid>
                {scoringForAnEmployee.map(data => <EmployeeDataRow key={data.id} {...data} />)}
            </Grid>
        </GridWrapper>    
   )
}

ScoringTable.propTypes = {
    scoringForAnEmployee: PropTypes.array
}

export default ScoringTable
  
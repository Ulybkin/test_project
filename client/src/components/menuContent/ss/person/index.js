import React from 'react'
import PropTypes from 'prop-types'

import { Container } from "./styled"

import MaterialButton from "../../../../common/button"


const Person = ({number, fullName, position, status}) => {
    return(
        <Container>
            <div className="fullNameBlock">
                <p>[{number}] &nbsp; <span>{fullName}</span></p>
            </div>
            <div>
                <div className="selectorBlock">
                    <span className="position">{position}</span>
                    <span className="status">{status}</span>
                </div>
                <div className="buttonsBlock">
                    <MaterialButton text="Войти" size="small" />
                </div>
            </div>
        </Container>
   )
}

Person.propTypes = { 
    person: PropTypes.shape({
        number: PropTypes.string,
        fullName: PropTypes.string,
        position: PropTypes.string,
        status: PropTypes.string
    })
}


export default Person
  
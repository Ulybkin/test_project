import styled from "styled-components";

export const Container =  styled.div`
    font-family: sans-serif;
    display: flex;
    flex-flow: row nowrap;
    justify-content: space-between;
    & .fullNameBlock {
        margin-right: 10px;
        & p {
            margin: 0;
            & span {
                font-weight:bold
            }
        }  
    }
    & div {
        display: flex;
        flex-flow: row nowrap;
        width: 62%;
        & .selectorBlock {
            display:flex;
            flex-direction: column;
            align-self: flex-start;
            margin-top: 5px;
            margin-right: 10px;
            & span {
                border-radius: 4px;
                text-align: center;
                color: white;
                line-height: 16px;
            }
            & .position {
                background-color: #307bb8;
                margin-bottom: 7px;
            }
            & .status {
                background-color: #5cb95c;
            }
        }

        & .buttonsBlock {
            display: flex;
            flex-direction: column;
            max-width: 100px;
        }
    }

    @media (max-width: 820px) {
        font-size: 12px
    }

    @media (max-width: 650px) {
        font-size: 12px
    }

    @media (max-width: 600px) {
        flex-flow: column nowrap;
        justify-content: center;
        align-items: center;
        font-size: 14px;
        min-width: 370px;
        
        & .fullNameBlock {
            min-width: 270px;
            & p {
                text-aling: center;
            }
        }
        & div {
            display: flex;
            flex-flow: column nowrap;
            width: 155px;
            
            & .buttonsBlock {
                margin-top: 7px;
                margin-bottom: 7px;
                margin: 0 auto;
                & button {
                    font-size: 10px;
                }
            }
        }
    }



`;

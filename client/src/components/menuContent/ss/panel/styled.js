import styled from "styled-components";

export const Container =  styled.div`
    font-family: sans-serif;

    h1 {
        text-align: center;
        color: #222;
    }

    a {
        display: block;
        text-align: center;
        color: #222;
    }


    
    width: 99%;
    border: 1px solid #ccc;
    box-shadow: 2px 2px 5px rgba(0, 0, 0, 0.3);
    border-radius: 3px;
    background-color: white;

    & > div {
    line-height: 2em;
    padding: 0px 20px;
    }

    .detail {
        flex-flow: column;
        padding: 0;
    }
    .summary {
        padding: 0;
        & div {
            margin: 5px 0;
        }
    }
      
`;



  

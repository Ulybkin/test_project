import React from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import { Container } from "./styled"
import Person from '../person'
import ScoringTable from '../scoringTable'
import Divider from '@material-ui/core/Divider'
import Info from '../info'
import {
  getPeopleById, 
  getCompanyInfoById,
  getScoringById
} from '../../../../selectors/menuSs'

import ExpansionPanel from '@material-ui/core/ExpansionPanel';
import ExpansionPanelSummary from '@material-ui/core/ExpansionPanelSummary';
import ExpansionPanelDetails from '@material-ui/core/ExpansionPanelDetails';

const Panel = ({ssPeople, companyInfo, scoringForAnEmployee}) => {
  return( 
    <Container>
      <ExpansionPanel>
        <ExpansionPanelSummary className="summary">
          <Info label={companyInfo.label} statistics={companyInfo.statistics} />
        </ExpansionPanelSummary>
        <ExpansionPanelDetails className="detail">
          {ssPeople.map((ppl, index) => ([
            <Person key={ppl.id} {...ppl}/>, 
            <Divider key={index}/>
          ]))}
          <ScoringTable scoringForAnEmployee = {scoringForAnEmployee}/>
        </ExpansionPanelDetails>
      </ExpansionPanel>
    </Container>
 )
}

Panel.propTypes = {
  //connect
  ssPeople: PropTypes.array,
  companyInfo: PropTypes.object,
  scoringForAnEmployee: PropTypes.array,
}

const mapStateToProps = (state, ownProps) => {
  return {
    ssPeople: getPeopleById(state, ownProps.id),
    companyInfo: getCompanyInfoById(state, ownProps.id),
    scoringForAnEmployee: getScoringById(state, ownProps.id)
  }
}

export default connect(mapStateToProps)(Panel)

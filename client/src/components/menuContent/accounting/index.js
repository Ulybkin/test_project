import React, {Component} from 'react'
import PropTypes from 'prop-types';
import { connect } from 'react-redux'

import { Container } from "./styled"
import { fetchDataForGraths } from '../../../actions/accounting'
import { getgrathsData } from '../../../selectors/accounting'
import Grath from "../../../common/grath"

class Accounting extends Component {
  componentDidMount() {
    this.props.fetchDataForGraths()
  }

  render() {
    const { grathsData } = this.props
    return( 
      <Container>
        {grathsData.map((data) => (<Grath key={data.id} {...data}/>))}
      </Container>
   )
  }
}

Accounting.propTypes = {
  //connect
  grathsData: PropTypes.array
} 

const mapStateToProps = (state) => {
  return {
    grathsData: getgrathsData(state)
  }
}
 
export default connect(mapStateToProps, {fetchDataForGraths})(Accounting)
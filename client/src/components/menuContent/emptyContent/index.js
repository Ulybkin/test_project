import React, {Component} from 'react';
import Styles from '../serviceWorks/styles'
import { Form, Field } from 'react-final-form'


const sleep = ms => new Promise(resolve => setTimeout(resolve, ms))

const onSubmit = async values => {
  await sleep(300)
  window.alert(JSON.stringify(values, 0, 2))
}



class EmptyContent extends Component {
  render() {

    return(
      <Styles>
        <Form
          onSubmit={onSubmit}
          initialValues={{ employed: true, stooge: 'larry' }}
          render={({ handleSubmit, reset, submitting, pristine, values }) => (
            <form onSubmit={handleSubmit}> 
              <div>
                <label>ФИО владельца(полностью)</label>
                <Field
                  name="FIO"
                  component="input"
                  type="text"
                  placeholder="Иванов Иван Иванович"
                />
              </div>
            </form>
          )}
        />
      </Styles>
   )
  }
}


  export default EmptyContent
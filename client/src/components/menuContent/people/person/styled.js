import styled from "styled-components";

export const Container =  styled.div`
    font-family: sans-serif;
    display: flex;
    flex-flow: row nowrap;
    justify-content: space-between;
    & .fullNameBlock {
        & p {
            margin: 0;
            & span {
                font-weight:bold
            }
        }
    }
    & div {
        display: flex;
        flex-flow: row nowrap;
        width: 270px;
        & .selectorBlock {
            display:flex;
            flex-direction: column;
            align-self: flex-start;
            margin-top: 5px;
            margin-right: 10px;
            max-width: 160px;
            & > span {
                background-color: blue;
                border-radius: 4px;
                text-align: center;
                color: white;
                line-height: 14px;
            }
            & > div {
                max-width: 158px;
                border: 1px solid rgba(0, 0, 0, 0.23);
                border-radius: 3px; 
            }
        }
        & .buttonsBlock {
            display: flex;
            flex-flow: column nowrap;
            max-width: 100px;

            & button {
                margin: 3px;
            }
        }
    }

    @media (max-width: 650px) {
        font-size: 12px
    }


    @media (max-width: 600px) {
        flex-flow: column nowrap;
        justify-content: center;
        align-items: center;
        font-size: 14px;
        min-width: 350px;
        
        & .fullNameBlock {
            min-width: 230px
        }
        & div {
            display: flex;
            flex-flow: column nowrap;
            width: 155px;
            & .selectorBlock {
                
            }
            & .buttonsBlock {
                display: flex;
                flex-direction: row;
                max-width: 100px;
                margin-left: -30px;
                margin-top: 7px;
                margin-bottom: 7px;
                & button {
                    font-size: 10px;
                }
            }
        }
    }
    
    
  

`;

import React, {Component} from 'react'
import PropTypes from 'prop-types';
import { Container } from "./styled"
import MaterialButton from "../../../../common/button"
import NativeSelect from '@material-ui/core/NativeSelect';
import Input from '@material-ui/core/Input';

class Person extends Component {
    state = {
        status: "active"
    }

    handleChange = (e) => {
        this.setState({ status: e.target.value });
    }

    handleClick = (e) => {
        console.log("click!")
    }

    render() { 
        const {
            number,
            fullName,
            position
        } = this.props.person
        return(
            <Container>
                <div className="fullNameBlock">
                    <p>[{number}] &nbsp; <span>{fullName}</span></p>
                </div>
                <div>
                    <div className="selectorBlock">
                        <span>{position}</span>
                        <NativeSelect
                            value={this.state.status}
                            onChange={this.handleChange}
                            input={<Input name="status" />}
                        >
                            <option value="" />
                            <option value={"active"}>Активен</option>
                            <option value={"disable"}>Отключен</option>
                        </NativeSelect>
                    </div>
                    <div className="buttonsBlock">
                        <MaterialButton text="Войти" size="small" />
                        <MaterialButton text="Удалить" size="small" />
                        <MaterialButton text="Обнулить" size="small" />
                    </div>
                </div>
            </Container>
    )
  }
}

Person.propTypes = {
    number: PropTypes.string,
    fullName: PropTypes.string,
    position: PropTypes.string
}


export default Person
  
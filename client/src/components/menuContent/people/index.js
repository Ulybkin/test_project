import React, {Component} from 'react'
import { connect } from 'react-redux'
import PropTypes from 'prop-types';


import { Container } from './styled'
import Person from './person'
import MaterialButton from "../../../common/button"
import { fetchMenuPeople } from '../../../actions/menuPeople'

import Divider from '@material-ui/core/Divider'


const sleep = ms => new Promise(resolve => setTimeout(resolve, ms))

const onSubmit = async values => {
  await sleep(300)
  window.alert(JSON.stringify(values, 0, 2))
}


class People extends Component {
  componentDidMount() {
    this.props.fetchMenuPeople()
  }

  render() {
    const { menuPeople } = this.props
    const personBlocksControl = menuPeople.map((ppl, index) => ([<Person key={ppl.id} person={ppl}/>, <Divider key={ppl.number}/>]))
    return(
      <Container>
          <div className="upperBlock">
            <div>
              <span className='fio'>ФИО</span>
            </div>
            <div>
              <MaterialButton text="Фильтры" size="small" />
            </div>
          </div>
          <Divider/>
          {personBlocksControl}
      </Container> 
   )
  }
}

People.propTypes = {
  //redux
  menuPeople: PropTypes.array
}

const mapStateToProps = (state) => {
  return {
    menuPeople: state.menuPeople
  }
}

export default connect(mapStateToProps, {fetchMenuPeople})(People)

  
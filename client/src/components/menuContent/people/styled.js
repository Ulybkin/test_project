import styled from "styled-components";

export const Container =  styled.div`
  font-family: sans-serif;

  h1 {
    text-align: center;
    color: #222;
  }

  & > div {
    text-align: center;
  }

  a {
    display: block;
    text-align: center;
    color: #222;
  }

  display: grid;
  grid-template-columns: minmax(320px, 750px);
  justify-content: center;


  border: 1px solid #ccc;
  border-radius: 3px;
  background-color: white;

  & > div {
    line-height: 2em;
    margin: 5px;
    padding: 0px 20px;

  }

  .upperBlock{ 
    display: flex;
    flex-flow: row nowrap;
    justify-content: space-between; 
    align-items: center;
  } 
  
  @media (max-width: 855px) {
    margin-top: 40px;
  }

  @media (max-width: 600px) {
    min-width: 350px;
    & > div {
      padding: 0px;
    }
    & .fio {
      display: none;
    }
  }
`;



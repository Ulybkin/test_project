import { Route, Switch } from "react-router"
import React, { Component } from "react"
import PropTypes from 'prop-types';

//import { Container } from "./styled"
import './index.css'
import ServiceWorks from "./serviceWorks/index"
import EmptyContent from "./emptyContent/index"
import People from "./people"
import Orders from "./orders"
import Ss from "./ss"
import Accounting from "./accounting"




export default class MenuContent extends Component {
    render() {
      let className = "general"
      if(this.props.open) className += " menuOpened"
      return (
        
          <div className= {className}>
            <Switch>
              <Route path="/serviceWorks" exact component={ServiceWorks} />
              <Route path="/people" exact component={People} />
              <Route path="/ss" exact component={Ss} />
              <Route path="/orders" exact component={Orders} />
              <Route path="/accounting" exact component={Accounting} />
              <Route component={EmptyContent} />
            </Switch>
          </div>
        
      );
    }
}

MenuContent.propTypes = {
  open: PropTypes.bool
}
  

// This file is shared across the demos.

import React from 'react'
import {Link} from 'react-router-dom'
import ListItem from '@material-ui/core/ListItem'
import ListItemText from '@material-ui/core/ListItemText'
import {mainNavItems} from '../../data'

const renderNavItems = () => mainNavItems.map(item => (
  <ListItem key={item.id} button component={Link} to={item.link}>
    <ListItemText primary={item.text} />
  </ListItem>
));

export const mailFolderListItems = (
  <div>
    {renderNavItems()}
  </div>
);






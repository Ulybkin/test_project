// @flow
import * as _ from 'lodash'

/* const ordersInfoGetter = state => 
    _.get(state, ['orders', 'ordersInfo'])

const idGetter = (state, props) => props.id */

export const goodInfoByIdGetter = (state, id) => 
    _.get(state, ['orders', 'goodsInfo', id])

export const getOrderIds = (state) =>
  _.get(state, ['orders', 'orderIds']) || []

export const getOrderInfoById = (state, id) => 
    _.get(state, ['orders', 'ordersInfo', id])

export const customerFullNameGetter = (state, id) => 
    _.get(state, ['orders', 'customers', id, 'fullName'])

/* export const getPeopleById = (state, id) => 
_.get(state, ['menuSs', 'ssPeople', id])


export const getCompanyInfoById = (state, id) => 
_.get(state, ['menuSs', 'companyInfo', id])

export const getScoringById = (state, id) => 
_.get(state, ['menuSs', 'scoringForAnEmployee', id]) */



/* export const goodSelectorFactory = () => createSelector(
    ordersInfoGetter, 
    idGetter, 
    goodsInfoGetter, 
    (ordersInfo, id, goodsInfo) => {
        const order = ordersInfo && ordersInfo[id] 

    return comments.get(id)
}) */

// @flow
import * as _ from 'lodash'

export const getgrathsData = (state) =>
  _.get(state, 'accounting') || []
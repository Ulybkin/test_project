import { FETCH_PEOPLE, FETCH_PEOPLE_SUCCEEDED } from '../constants/menu'

export const fetchMenuPeople = () => ({
  type: FETCH_PEOPLE
});

export const fetchMenuPeopleSucceed = (data) => ({
    type: FETCH_PEOPLE_SUCCEEDED,
    payload: data
  });

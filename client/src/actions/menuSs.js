import {  FETCH_SS_DATA, FETCH_SS_DATA_SUCCEEDED } from '../constants/menu'

export const fetchSsData= () => ({
  type: FETCH_SS_DATA
});

export const fetchSsDataSucceed = (data) => ({
    type: FETCH_SS_DATA_SUCCEEDED,
    payload: data
});



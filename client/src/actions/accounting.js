import { FETCH_DATA_FOR_GRATHS, FETCH_DATA_FOR_GRATHS_SUCCEEDED } from '../constants/menu'

export const fetchDataForGraths= () => ({
  type: FETCH_DATA_FOR_GRATHS
});

export const fetchDataForGrathsSucceed = (data) => ({
    type: FETCH_DATA_FOR_GRATHS_SUCCEEDED,
    payload: data
});



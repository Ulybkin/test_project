import { FETCH_ORDERS, FETCH_ORDERS_SUCCEEDED } from '../constants/menu'

export const fetchOrders= () => ({
  type: FETCH_ORDERS
});

export const fetchOrdersSucceed = (data) => ({
    type: FETCH_ORDERS_SUCCEEDED,
    payload: data
});



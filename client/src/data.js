export const mainNavItems = [
    {
        "id": "1",
        "text": "Серийники",
        "link": "/serial"
    },
    {
        "id": "2",
        "text": "Приглашение для СЦ",
        "link": "/invite"
    },
    {
        "id": "3",
        "text": "Люди",
        "link": "/people"
    },
    {
        "id": "4",
        "text": "СЦ",
        "link": "/ss"
    },
    {
        "id": "5",
        "text": "Оборудование клиентов",
        "link": "/customerEquipment"
    },
    {
        "id": "6",
        "text": "Монтажи",
        "link": "/mountings"
    },
    {
        "id": "7",
        "text": "Сервисные работы",
        "link": "/serviceWorks"
    },
    {
        "id": "8",
        "text": "Заказы",
        "link": "/orders"
    },
    {
        "id": "9",
        "text": "Профиль",
        "link": "/profile"
    },
    {
        "id": "10",
        "text": "Статистика и отчёты",
        "link": "/accounting"
    },
    {
        "id": "11",
        "text": "Настройки",
        "link": "/settings"
    },
    {
        "id": "12",
        "text": "Выход",
        "link": "/exit"
    }
];


/* Highcharts.chart('container', {

    title: {
        text: 'Solar Employment Growth by Sector, 2010-2016'
    },



    legend: {
        layout: 'vertical',
        align: 'right',
        verticalAlign: 'middle'
    },

    plotOptions: {
        series: {
            label: {
                connectorAllowed: false
            },
            pointStart: 2010
        }
    },

    series: [{
        name: 'Installation',
        data: [43934, 52503, 57177, 69658, 97031, 119931, 137133, 154175]
    }, {
        name: 'Manufacturing',
        data: [24916, 24064, 29742, 29851, 32490, 30282, 38121, 40434]
    }, {
        name: 'Sales & Distribution',
        data: [11744, 17722, 16005, 19771, 20185, 24377, 32147, 39387]
    }, {
        name: 'Project Development',
        data: [null, null, 7988, 12169, 15112, 22452, 34400, 34227]
    }, {
        name: 'Other',
        data: [12908, 5948, 8105, 11248, 8989, 11816, 18274, 18111]
    }],

    responsive: {
        rules: [{
            condition: {
                maxWidth: 500
            },
            chartOptions: {
                legend: {
                    layout: 'horizontal',
                    align: 'center',
                    verticalAlign: 'bottom'
                }
            }
        }]
    }

});
 */
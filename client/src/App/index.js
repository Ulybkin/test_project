import React, { Component } from 'react';
import './style.css';
import PersistentDrawer from '../components/drawer';
import MenuContent from '../components/menuContent';


export default class App extends Component {
  
  state = {
    open:false
  }

  handleToggleMenu = (value) => {
    this.setState({open: value})
  }

  render() {
    const {open} = this.state
    return (
      <div className="App">
        <PersistentDrawer handleToggleMenu={this.handleToggleMenu} open={open}/>
        <MenuContent open={open}/>
      </div>
    );
  }
}


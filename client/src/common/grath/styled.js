import styled from "styled-components";

export const Container =  styled.div`
    .highcharts-credits {
        display: none;
    }

    .highcharts-legend {
        display: none;
    }
`;
import React from 'react'
import HighchartsReact from './highchartsReact'
import Highcharts from 'highcharts'
import {Container} from './styled'

const Grath =  ({textAxisX, textAxisY, categories, points}) => {
    return (
      <Container>
        <HighchartsReact
          highcharts={Highcharts}
          options={{ 
            title: {
                text: textAxisX
            },

            xAxis: {
                //min: 1,
                categories: categories
            },
            yAxis: {
                title: {
                    text: textAxisY
                }
            },
        
            plotOptions: {
                series: {
                    label: {
                        connectorAllowed: false
                    },
                }
            },
        
            series: points,
           }}
          constructorType={'chart'}
        />
      </Container>

    )
  
}

export default Grath
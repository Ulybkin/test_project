import React, {Component} from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Button from '@material-ui/core/Button';

const styles = theme => ({
  button: {
    margin: theme.spacing.unit,
    backgroundColor: "white",
  }
});

class MaterialButton extends Component {

  handleClick = () => {
    console.log("hello")
  }

  render() {
    const {classes, text, size} = this.props
    return (
      <Button 
          size={size} 
          className={classes.button}
          variant="outlined"
          onClick={this.handleClick}
      >
          {text}
      </Button>
    )
  }  
}
  


MaterialButton.propTypes = {
  classes: PropTypes.object.isRequired,
  text: PropTypes.string.isRequired,
  size: PropTypes.string.isRequired,
};

export default withStyles(styles)(MaterialButton);
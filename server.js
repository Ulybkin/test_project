const express = require('express');

const app = express();

app.get('/api/people', (req, res) => {
  const people = [
    {
        id: "qwerweerw",
        number: "4179",
        fullName: "Ацаева Надина Ромзановна",
        position: "Владелец"
    },
    {
        id: "ffgdfdsds",
        number: "4757",
        fullName: "Валяев Николай Альбертович",
        position: "Владелец"
    },
    {
        id: "dfsdfsdf",
        number: "1892",
        fullName: "Гордубалов Энакин Скайвокер",
        position: "Сотрудник сервисного центра"
    },
    {
        id: "sdfeerwer",
        number: "3412",
        fullName: "Мзарев Иосиф Кудинович",
        position: "Владелец"
    },
    {
        id: "hgbdsgsdfg",
        number: "1234",
        fullName: "Анягина Лидия Петровна",
        position: "Владелец"
    },
]
  res.json(people);
});

app.get('/api/ssData', (req, res) => {
    const data = {
        companyIds: ["company1id", "company2id"],
        ssPeople: {
            company1id: [
                {
                    id: "qwerdddeerw",
                    number: "65",
                    fullName: "Скосырская Наталья Ромзановна",
                    position: "Администратор",
                    status: "Активен"
                },
                {
                    id: "ffgdfdfsdfsds",
                    number: "335",
                    fullName: "Килов Николай Альбертович",
                    position: "Сотрудник",
                    status: "Активен"
                },
                {
                    id: "dfsdfdsssdf",
                    number: "486",
                    fullName: "Кудрин Александр Самойлович",
                    position: "Сотрудник",
                    status: "Активен"
                },
                {
                    id: "sdfeffgerwer",
                    number: "1285",
                    fullName: "Улаев Павел Кудинович",
                    position: "Сотрудник",
                    status: "Активен"
                },
                {
                    id: "hgbdsdfssgsdfg",
                    number: "1287",
                    fullName: "Линкина Светлана Петровна",
                    position: "Сотрудник",
                    status: "Активен"
                },
                {
                    id: "sdfeffgegjdfrwer",
                    number: "1205",
                    fullName: "Улаев Павел Кудинович",
                    position: "Сотрудник",
                    status: "Активен"
                },
                {
                    id: "hgbdsdfsewazdsgsdfg",
                    number: "14",
                    fullName: "Линкина Светлана Петровна",
                    position: "Сотрудник",
                    status: "Активен"
                },
            ],
            company2id: [
                {
                    id: "qwerddddddeerw",
                    number: "64",
                    fullName: "Скосырская Наталья Ромзановна",
                    position: "Администратор",
                    status: "Активен"
                },
                {
                    id: "ffgdfddfsdfsds",
                    number: "3535",
                    fullName: "Килов Николай Альбертович",
                    position: "Сотрудник",
                    status: "Активен"
                },
                {
                    id: "dfsdfdsssssdf",
                    number: "483",
                    fullName: "Кудрин Александр Самойлович",
                    position: "Сотрудник",
                    status: "Активен"
                },
                {
                    id: "sdfeffegerwer",
                    number: "125",
                    fullName: "Улаев Павел Кудинович",
                    position: "Сотрудник",
                    status: "Активен"
                },
                {
                    id: "hgbdsdrrfssgsdfg",
                    number: "127",
                    fullName: "Линкина Светлана Петровна",
                    position: "Сотрудник",
                    status: "Активен"
                },
            ] 
        },
        scoringForAnEmployee: {
            company1id: [
                {   
                    id: "dfsssddd",
                    date : "2016-12-05 12:45:16",
                    empoyee: "Скотников Константин Васильевич",
                    description: "20 за Vaillant VU 362/5-5. Внеплановый сервис/Разовое ТО",
                    score: 20
                },
                {
                    id: "ddfsssasdsdd",
                    date : "2016-12-05 12:49:07",
                    empoyee: "Скотников Константин Васильевич",
                    description: "10 за Vaillant VUW INT IV 346/5-3. Аварийный выезд по договору",
                    score: 10
                },
            ],
            company2id: [
                {   
                    id: "dfsssdfsssddd",
                    date : "2016-12-08 12:25:16",
                    empoyee: "Скотников Константин Васильевич",
                    description: "30 за Vaillant VU 362/5-5. Внеплановый сервис/Разовое ТО",
                    score: 30
                },
                {
                    id: "ddfsssfsdfasdsdd",
                    date : "2016-12-09 12:59:07",
                    empoyee: "Скотников Константин Васильевич",
                    description: "15 за Vaillant VUW INT IV 346/5-3. Аварийный выезд по договору",
                    score: 15
                },
            ],
        },
        companyInfo: {
            company1id: 
                {   
                    id: "assddss",
                    label : "[2604] OOO Fiexi",
                    statistics: "7 человек, 65335 баллов",
                },
            company2id: 
                {   
                    id: "assddfsdsss",
                    label : "[12341] OOO Компания 'Святогор'",
                    statistics: "5 человек, 44335 баллов",
                },
        }
    }
   
  res.json(data);
})

app.get('/api/orders', (req, res) => {
    const orders = {
        customers: {
            "customerId1": {
                fullName: "Левченко Виктор Сергеевич"
            },
            "customerId2": {
                fullName: "Грачёв Павел Валерьевич"
            },
            "customerId3": {
                fullName: "Денисев Виталий Юрьевич"
            },
            "customerId4": {
                fullName: "Хисамутдинов Денис Наилевич"
            }
        },
        goodsInfo: {
            "1111": {
                description: 'Автомобильное зарядное уствойство USB',
                score: 170,
                type: 'car_charger'
            },
            "2222": {
                description: 'Инструмент настройки газового клапана SIT',
                score: 400,
                type: "klyuch"
            },
            "3333": {
                description: 'Отвёртка TORX',
                score: 170,
                type: "screwdriver"
            },
        },
        ordersInfo: {
            "1534": 
                {   
                    date : "2018-05-25 13:33:42",
                    customerId: "customerId1",
                    goods: ["1111"]
            },
            "1533": 
                {   
                    date : "2018-05-25 08:28:31",
                    customerId: "customerId2",
                    goods: ["2222"]
            },
            "1530": 
                {   
                    date : "2018-05-23 19:08:26",
                    customerId: "customerId3",
                    goods: ["3333"]
            },
            "1531": 
                {   
                    date : "2018-05-23 08:32:02",
                    customerId: "customerId4",
                    goods: ["2222", "2222", "2222"]
            },
        },
        orderIds: ["1534", "1533", "1530", "1531"]
    }

    res.json(orders);
  });

  app.get('/api/dataForGraths', (req, res) => {
    const graths = [
      {
        id: "qwerwe44erw",
        textAxisX: 'Количество пользователей',
        textAxisY: 'Пользователи',
        categories: ['Июль 2016', 'Окт 2016', 'Янв 2017', 'Апр 2017', 'Июль 2017', 'Окт 2017', 'Янв 2018', 'Апр 2018'],
        points: [{
            name: "Линия 1",
            data: [0, 350, 500, 1000, 1600, 3100, 3900, 4500]
        }, {
            name: 'Линия 2',
            data: [0, 0, 50, 500, 1000, 2400, 2950, 3500]
        }, {
            name: 'Линия 3',
            data: [0, 100, 150, 200, 250, 400, 500, 600]
        }, {
            name: 'Линия 4',
            data: [0, 50, 70, 120, 150, 200, 250, 250]
        }, {
            name: 'Линия 5',
            data: [0, 10, 20, 30, 30, 30, 30, 30]
        }]
      },
      {
        id: "qwerwe54erw",
        textAxisX: 'Количество событий',
        textAxisY: 'События',
        categories: ['Июль 2016', 'Окт 2016', 'Янв 2017', 'Апр 2017', 'Июль 2017', 'Окт 2017', 'Янв 2018', 'Апр 2018'],
        points: [{
            name: "Линия 1",
            data: [0, 4000, 7000, 12000, 20000, 34000, 45000, 51000]
        }, {
            name: 'Линия 2',
            data: [0, 0, 0, 0, 700, 2000, 2400, 2500]
        }]
    },
     
  ]
    res.json(graths);
  });

const port = 5000;

app.listen(port, () => `Server running on port ${port}`);